# Project Generator

_Based on the [Spring Initializr](https://github.com/spring-io/initializr) project._

Generates a default Spring-boot/Java or Spring-boot/Groovy project, based on input you provide. The project
will be pre-configured to integrate with the [Product Continuous Integration process](https://mttnow.atlassian.net/wiki/spaces/PMO2/pages/647889028/CI+CD+Documentation),
and will use the latest TDL dependencies.

## Future plans
We plan on integrating other project types in the future, to provide the facility to generate
* NodeJS projects
* Angular JS web application projects
* Android projects
* iOS projects

## Generating a project
If you click on "Generate Project" on the web ui of our instance, it will download a
project archive with a Maven-based project and the necessary infrastructure to start
the app.

You could achieve the same result with a simple `curl` command

```bash
$ curl https://<your address>/starter.zip -o demo.zip
```

The web ui exposes a bunch of options that you can configure. These are mapped to the
following request attributes:

* Basic information for the generated project: `groupId`, `artifactId`, `version`,
`name`, `description` and `packageName`
** The `name` attribute is also used to generate a default application name. The
logic is that the name of the application is equal to the `name` attribute with an
`Application` suffix (unless said suffix is already present). Of course, if the
specified name contains an invalid character for a java identifier, `Application` is
used as fallback.
** The `artifactId` attribute not only defines the identifier of the project in the
build but also the name of the generated archive.
* `dependencies` (or `style`): the identifiers of the dependencies to add to the
project. Such identifiers are defined through configuration and are exposed in the [metadata](https://github.com/spring-io/initializr#metadata).
* `type`: the _kind_ of project to generate (e.g. `maven-project`). Again, each
service exposes an arbitrary number of supported types and these are available in the [metadata](https://github.com/spring-io/initializr#metadata).
* `javaVersion`: the language level (e.g. `1.8`).
* `bootVersion`: the Spring Boot version to use (e.g. `1.2.0.RELEASE`).
* `language`: the programming language to use (e.g. `java`).
* `packaging`: the packaging of the project (e.g. `jar`).
* `applicationName`: the name of the application class (inferred by the `name`
attribute by default).
* `baseDir`: the name of the base directory to create in the archive. By default, the
project is stored in the root.

This command generates an `another-project` directory holding a Gradle web-based
Groovy project using the actuator:

```bash
$ curl https://.../starter.tgz \
    -d dependencies=web,actuator \
    -d language=groovy \
    -d type=gradle-project \
    -d baseDir=another-project \
    | tar -xzvf -
```

NOTE: The `/starter.tgz` endpoint offers the same feature as `/starter.zip` but
generates a compressed tarball instead.

You could use this infrastructure to create your own client since the project is
generated via a plain HTTP call.

## Customize form inputs

You can share or bookmark URLs that will automatically customize form inputs. For
instance, the following URL from the default instance uses `groovy` by default and
set the name to `Groovy Sample`:

```bash
https://start.spring.io/#!language=groovy&name=Groovy%20Sample
```

The following hashbang parameters are supported: `type`, `groupId`, `artifactId`,
`name`, `description`, `packageName`, `packaging`, `javaVersion` and `language`.
Review the section above for a description of each of them.

## Running your own instance

You can easily run your own instance. The `initializr-web` modules uses Spring Boot
so when it is added to a project, it will trigger the necessary auto-configuration to
deploy the service.

You first need to create or update your configuration to define the necessary
attributes that your instance will use. Again, check the documentation for a
[description of the configuration](https://docs.spring.io/initializr/docs/current-SNAPSHOT/reference/htmlsingle/#configuration-format) and
[review our own config](https://docs.spring.io/initializr/docs/current-SNAPSHOT/reference/initializr-service/application.yml) for a sample.

You can integrate the library in a traditional Java-based project or by writing the
super-simple script below:

```groovy
package org.acme.myapp

@Grab('io.spring.initializr:initializr-web:1.0.0.BUILD-SNAPSHOT')
@Grab('spring-boot-starter-web')
class YourInitializrApplication { }
```

NOTE: Spring Initializr is not available on Maven central yet so you will have to
build it from source in order to use it in your own environment.

Once you have created that script (`my-instance.groovy`), place your configuration
in the same directory and simply execute this command to start the service:

```bash
$ spring run my-instance.groovy
```

You may also want to run the default instance locally.

## Building from Source

You need Java 1.8 and a bash-like shell.

### Building

Just invoke the build at the root of the project

```bash
    $ ./mvnw clean install
```

If you want to run the smoke tests using Geb, you need to enable the
`smokeTests` profile. Firefox should also be installed on your machine:

```bash
    $ ./mvnw verify -PsmokeTests
```

If you want to build both the library and the service, you can enable the `full`
profile:

```bash
    $ ./mvnw clean install -Pfull
```

### Running the app locally

Once you have built the library, you can easily start the app as any
other Spring Boot app from the `initializr-service` directory:

```bash
    $ cd initializr-service
    $ ../mvnw spring-boot:run
```

### Running the app in an IDE

You should be able to import the projects into your IDE with no problems. Once there you
can run the `initializr-service` from its main method, debug it, and it will reload if
you make changes to other modules. (You may need to manually enable the "full" profile.)
This is the recommended way to operate while you are developing the application,
especially the UI.


## Deploying to Cloud Foundry

If you are on a Mac and using [homebrew](http://brew.sh/), install the Cloud Foundry
CLI:

```bash
    $ brew install cloudfoundry-cli
```

Alternatively, download a suitable binary for your platform from
[Pivotal Web Services](https://console.run.pivotal.io/tools).

You should ensure that the application name and URL (name and host values) are
suitable for your environment before running `cf push`.

First, make sure that you have built the library, then make sure first
that the jar has been created:

```bash
    $ cd initializr-service
    $ ../mvnw package
```

Once the jar has been created, you can push the application:

```bash
    $ cf push your-initializr -p target/initializr-service.jar
```

## License
Spring Initializr is Open Source software released under the
[Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0.html).
