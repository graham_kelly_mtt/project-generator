package {{packageName}}.repository

import {{packageName}}.model.Person
import org.springframework.data.mongodb.repository.MongoRepository

interface PersonRepository : MongoRepository<Person, String> {
  fun findByFirstName(firstName: String): List<Person>?
}
