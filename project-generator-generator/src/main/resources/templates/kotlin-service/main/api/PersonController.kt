package {{packageName}}.api

import {{packageName}}.model.Person
import {{packageName}}.service.PersonService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

import javax.validation.Valid

@RestController
@RequestMapping("/persons")
class PersonController {
  //Only set on first access
  private val log: Logger by lazy { LoggerFactory.getLogger(PersonController::class.java)}

  @Autowired
  private lateinit var personService: PersonService

  @GetMapping
  fun findAll() = personService.findAll()

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  fun addPerson(@Valid @RequestBody person: Person) {
    logger.debug("Adding a person - {}", person.id)
    personService.publish(person)
  }
}
