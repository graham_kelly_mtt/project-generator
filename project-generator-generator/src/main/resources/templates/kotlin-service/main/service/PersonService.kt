package {{packageName}}.service

import {{packageName}}.model.Person
import {{packageName}}.repository.PersonRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PersonService {
  //Only set on first access
  private val log: Logger by lazy { LoggerFactory.getLogger(PersonService::class.java)}

  @Autowired
  private lateinit var personRepository: PersonRepository

  @Autowired
  private lateinit var personPublisher: PersonPublisher

  fun count() = return personRepository.count()

  fun findAll() = personRepository.findAll()

  fun save(person: Person) {
    logger.debug("Saving Person id:{}", person.id)
    personRepository.save(person)
  }

  fun publish(person: Person) {
    logger.debug("Publishing Person id:{}", person.id)
    personPublisher.publish(person)
  }
}
