package {{packageName}}.service

import {{packageName}}.model.Person
import {{packageName}}.repository.PersonRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PersonService {

  @Autowired
  PersonRepository personRepository

  @Autowired
  PersonPublisher personPublisher

  long count() {
    return personRepository.count()
  }

  List<Person> findAll() {
    return personRepository.findAll()
  }

  void save(Person person) {
    personRepository.save(person)
  }

  void publish(Person person) {
    personPublisher.publish(person)
  }

}
