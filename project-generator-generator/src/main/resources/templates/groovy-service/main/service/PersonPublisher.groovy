package {{packageName}}.service

import {{packageName}}.model.Person
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class PersonPublisher {

  @Autowired
  RabbitTemplate personAmqpTemplate

  void publish(Person person) {
    personAmqpTemplate.convertAndSend(person)
  }

}
