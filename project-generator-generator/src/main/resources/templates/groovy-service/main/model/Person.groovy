package {{packageName}}.model

import groovy.transform.EqualsAndHashCode
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

import javax.validation.constraints.NotEmpty

@Document
@EqualsAndHashCode
class Person {

  @Id
  String id

  @NotEmpty
  String firstName
  String lastName
  int age

}
