package {{packageName}}.config

import com.mttnow.platform.spring.boot.auto.configure.rabbit.RabbitConfigurerAdapter
import {{packageName}}.model.Person
import groovy.transform.CompileStatic
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class RabbitConfig extends RabbitConfigurerAdapter {

  public static final String PERSON_QUEUE = "person.queue"
  public static final String PERSON_QUEUE_DEAD_LETTER = "person.queue.dl"
  public static final String PERSON_LISTENER_FACTORY = "personListenerFactory"

  @Bean
  Queue personQueue() {
    new Queue(PERSON_QUEUE, true)
  }

  @Bean
  Queue personDeadLetterQueue() {
    new Queue(PERSON_QUEUE_DEAD_LETTER, true)
  }

  @Bean
  RabbitTemplate personAmqpTemplate() {
    RabbitTemplate rabbitTemplate = createRabbitTemplate()

    rabbitTemplate.setRoutingKey(PERSON_QUEUE)
    rabbitTemplate.setMessageConverter(messageConverter(Person.class))

    rabbitTemplate
  }

  @CompileStatic
  @Bean(name = RabbitConfig.PERSON_LISTENER_FACTORY)
  SimpleRabbitListenerContainerFactory personListenerContainerFactory() {
    def factory = createContainerFactoryWithRetryInterceptor(personQueue(), personDeadLetterQueue())

    factory.setMessageConverter(messageConverter(Person.class))

    factory
  }

}
