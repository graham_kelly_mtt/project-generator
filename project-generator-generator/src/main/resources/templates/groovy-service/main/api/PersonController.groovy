package {{packageName}}.api

import {{packageName}}.model.Person
import {{packageName}}.service.PersonService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

import javax.validation.Valid

@RestController
@RequestMapping("/persons")
class PersonController {

  @Autowired
  PersonService personService

  @GetMapping
  List<Person> findAll() {
    personService.findAll()
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  void addPerson(@Valid @RequestBody Person person) {
    personService.publish(person)
  }

}


