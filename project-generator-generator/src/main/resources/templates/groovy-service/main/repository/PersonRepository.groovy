package {{packageName}}.repository

import {{packageName}}.model.Person
import org.springframework.data.mongodb.repository.MongoRepository

interface PersonRepository extends MongoRepository<Person, String> {

}
