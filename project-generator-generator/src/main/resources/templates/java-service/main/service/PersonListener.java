package {{packageName}}.service;

import {{packageName}}.config.RabbitConfig;
import {{packageName}}.model.Person;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonListener {

  @Autowired
  private PersonService personService;

  @RabbitListener(
    id = RabbitConfig.PERSON_QUEUE,
    queues = RabbitConfig.PERSON_QUEUE,
    containerFactory = RabbitConfig.PERSON_LISTENER_FACTORY
  )
  public void consume(Person person) {
    personService.save(person);
  }

}
